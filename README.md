 # Introduction<br>
<br>
Extend Exception handler to help displaying Exception errors
<br>
<br>
## Documentation<br>
<br>
Please refer to the online wiki:<br>
[wiki](https://gitlab.com/brunoocto/exception/wikis/home)
<br>
<br>
## Installation<br>
<br>
In your framework root directory, open composer.json and add the following repository:<br>
```json
    [...]
    "repositories": [
        [...]
        {
            "type": "vcs",
            "url":  "git@gitlab.com:brunoocto/exception.git"
        }
        [...]
    ]
    [...]
```
<br>
<br>
Then import the library via composer:<br>
```console
me@dev:# composer require brunoocto/exception:"~1.0"
```
<br>
<br>
## Tests<br>
<br>
Run all Feature tests, Unit tests, and generate a Coverage report:<br>
```console
me@dev:# phpunit
```
<br>
