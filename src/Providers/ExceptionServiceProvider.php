<?php

namespace Brunoocto\Exception\Providers;

use Dotenv\Dotenv;
use Illuminate\Support\ServiceProvider;
use Brunoocto\Exception\Services\ExceptionService;
use Orchestra\Testbench\Exceptions\Handler as OrchestraHandler;

class ExceptionServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Load environment variables specific to the library (default is .env)
        $dotenv = Dotenv::createMutable(__DIR__.'/../../');
        $dotenv->load();

        //Extend Exception Handler to display error message on console
        $this->app->singleton(OrchestraHandler::class, ExceptionService::class);
    }
}
