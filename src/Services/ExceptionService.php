<?php

namespace Brunoocto\Exception\Services;

use Throwable;
use Orchestra\Testbench\Exceptions\Handler as OrchestraHandler;

/**
 * Extend OrchestraHandler
 *
 */
class ExceptionService extends OrchestraHandler
{
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $e
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $e)
    {
        if (env('LINCKO_EXCEPTION_DISPLAY')) {
            // Add a echo line to easily debug while observing test
            echo "\n\n---- ERROR ----\n".get_class($e).': '.$e->getFile(). ' ['.$e->getLine().']'."\n".$e->getMessage()."\n---------------\n";
            if (env('LINCKO_EXCEPTION_TRACE')) {
                // Add a echo line to easily debug while observing test
                echo $e->getTraceAsString()."\n---------------\n";
            }
        }
        return parent::render($request, $e);
    }
}
