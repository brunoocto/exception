<?php

namespace Brunoocto\Exception\Tests\Feature;

use Exception;
use Brunoocto\Exception\Tests\TestCase;

class ExceptionFeatureTest extends TestCase
{
    /**
     * Test Exception method
     *
     * @return void
     */
    public function testException()
    {
        // Force to display the message
        $_ENV['LINCKO_EXCEPTION_DISPLAY'] = $_SERVER['LINCKO_EXCEPTION_DISPLAY'] = true;
        putenv('LINCKO_EXCEPTION_DISPLAY=true');
        // Force to display the stacktrace
        $_ENV['LINCKO_EXCEPTION_TRACE'] = $_SERVER['LINCKO_EXCEPTION_TRACE'] = true;
        putenv('LINCKO_EXCEPTION_TRACE=true');

        // Build a Route only for test
        \Route::get('/tests/feature/exception', function () {
            // This exception should return a 500 error
            throw new Exception('!!! IMPORTANT !!! => Test is ok if you see this exception in the console while running phunit.');
            // We should not go here
            return 'ok';
        });

        $response = $this->json(
            'GET',
            '/tests/feature/exception'
        );

        $response->assertStatus(500);
        $this->assertEquals(true, env('LINCKO_EXCEPTION_DISPLAY'));
    }
}
