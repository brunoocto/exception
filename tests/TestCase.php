<?php

namespace Brunoocto\Exception\Tests;

use Orchestra\Testbench\TestCase as TestbenchCase;
use Brunoocto\Exception\Providers\ExceptionServiceProvider;

/**
 * TestCase for Service Provider
 */
class TestCase extends TestbenchCase
{
    /**
     * Get package providers.
     * @param  Illuminate\Foundation\Application  $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            ExceptionServiceProvider::class,
        ];
    }
}
